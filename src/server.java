import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class server {

	private String filePath;

	public void setFile(String s) throws IOException {
		this.filePath = s;
		this.connect();
	}

	public void connect() throws IOException {

		// create a server socket
		ServerSocket serverSocket = new ServerSocket(6666);
		System.out.println("Server is ready at port : 6666");

		//wait for connection and accepts it
		Socket socket = serverSocket.accept();
		System.out.println("now is connected ... ");

		// get the file
		File transferFile = new File(this.filePath);
		
		//read the file
		byte[] bytearray = new byte[(int) transferFile.length()];
		FileInputStream fin = new FileInputStream(transferFile);
		BufferedInputStream bin = new BufferedInputStream(fin);
		bin.read(bytearray, 0, bytearray.length);
		
		// create a communication path to client 
		OutputStream os = socket.getOutputStream();
		
		// write to the client 
		os.write(bytearray, 0, bytearray.length);
		System.out.println("File was transeferded ...");
		
		// end and close the communication path
		os.flush();
		socket.close();
		System.out.println("connection is ended");

	}

}
