import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class client {

	private String filePath;
	private int filesize = 1022386;
	private int bytesRead;
	private int currentTot = 0;

	public void setFile(String s) throws UnknownHostException, IOException {
		this.filePath = s;
		this.connect();
	}

	public void connect() throws UnknownHostException, IOException {

		// create a socket with the host and port of the server
		Socket s = new Socket("localhost", 6666);
		System.out.println("Sockect is connected to server");

		// 
		byte[] bytearray = new byte[filesize];
		
		// make a communication channel 
		InputStream is = s.getInputStream();
		
		// write to the file
		FileOutputStream fos = new FileOutputStream(this.filePath);
		BufferedOutputStream bos = new BufferedOutputStream(fos);
		bytesRead = is.read(bytearray, 0, bytearray.length);
		currentTot = bytesRead;

		do {
			bytesRead = is.read(bytearray, currentTot, (bytearray.length - currentTot));
			if (bytesRead >= 0) {
				currentTot += bytesRead;
			}
		} while (bytesRead > -1);

		bos.write(bytearray, 0, currentTot);
		bos.flush();
		bos.close();
		s.close();

	}

}
